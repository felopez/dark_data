#!/bin/sh
set -e

smart_cat(){
    type=$(file -b $1 | awk '{print $1}')
    cmd=false
    case "$type" in
    bzip2)
        cmd=bzcat
        ;;
    gzip)
        cmd=zcat
        ;;
    xz)
        cmd=xzcat
        ;;
    ASCII)
        cmd=cat
        ;;
    *)
        echo "$type filetype unknown"
        exit 1
        ;;
    esac
    $cmd "$1"
}


se_dump=$1
rucio_dump=$2
prefix=$3

prefix="${prefix%%/}/"

se_dump_sorted=`mktemp`
rucio_dump_sorted=`mktemp`


smart_cat $se_dump | sed "s;^/*rucio/;;" | sed -r 's;^/+;;' | sort > $se_dump_sorted
smart_cat $rucio_dump | tr '\t' ',' | cut -d, -f7 | sed "s;^/*rucio/;;" | sed -r 's;^/+;;' | sort > $rucio_dump_sorted

if [ -z "$prefix" ]; then
	comm -23 $se_dump_sorted $rucio_dump_sorted 
else
	comm -23 $se_dump_sorted $rucio_dump_sorted | sed "s;^;${prefix};"
fi


rm -f $se_dump_sorted $rucio_dump_sorted
