#Script for extracting file information from dpm database and converting the result into xml
#Erming Pei, 2009/11/13
#Tomas Kouba, 2012/11/16
#Dennis van Dok, 2015/07/03
#Alessandra Forti, 2015/10/14, 2015/11/18

import sys,os
import datetime
import time
import MySQLdb
import MySQLdb.cursors
import logging
import re

from optparse import OptionParser
try: import simplejson as json
except ImportError: import json

default_ns_db = 'cns_db'

def guess_config_files():
    """
    """
    possible_nsconfigs = ['/opt/lcg/etc/NSCONFIG', '/usr/etc/NSCONFIG']
    if os.environ.has_key('LCG_LOCATION'):
       possible_nsconfigs.append(os.environ['LCG_LOCATION'].rstrip('/') + '/etc/NSCONFIG') 
    guess_nsconfig = possible_nsconfigs[0]
    for f in possible_nsconfigs:
        if os.path.exists(f):
            guess_nsconfig = f
    return guess_nsconfig

def get_conn_data(nsconfig, verbose):
    """ Returns connection data from NSCONFIG """
    retval = {}
    
    if verbose:
        sys.stderr.write("Getting connection info from %s\n" % nsconfig)
    try:
        nsconfig_line = open(nsconfig).readline().strip()
    except:
        sys.stderr.write("Cannot open DPM config file: %s\n" % nsconfig)
        sys.exit(-1)
#    nsre = re.compile("([a-zA-Z0-9]+)/([^@]+)@([a-zA-Z0-9.]+)/*([a-zA-Z0-9_]+)*")
#    nsre = re.compile("([a-zA-Z0-9]+)/([^@]+)@([a-zA-Z0-9.\-]+)/*([a-zA-Z0-9_]+)*")
    nsre = re.compile("(.*)/(.*)@(.*)/(.*)") 
    splitlist = nsre.split(nsconfig_line)
    #splitlist = [x.split('?') for x in nsconfig_line.split('@')]
    retval['ns_user'] = splitlist[1]
    retval['ns_pass'] = splitlist[2]
    retval['ns_host'] = splitlist[3]
    retval['ns_db'] = splitlist[4] if splitlist[4] else default_ns_db
    if verbose:
        sys.stderr.write("%s\n" % str(retval))
    return retval    

def open_xml_out(filename):
    return open(filename, "w")

def open_json_out(filename):
    return open(filename, "w")

def open_txt_out(filename):
    return open(filename, "w")

def dump_data(conn_data, options):

    curtime=datetime.datetime.isoformat(datetime.datetime.now())
    try:
        conn=MySQLdb.connect(host=conn_data['ns_host'], user=conn_data['ns_user'], passwd=conn_data['ns_pass'], db=conn_data['ns_db'], cursorclass = MySQLdb.cursors.SSCursor)
        sql="select fileid, parent_fileid,name,filesize,filemode,csumvalue,atime,mtime from Cns_file_metadata order by parent_fileid"            
        cursor=conn.cursor()
        cursor.execute(sql)

    except MySQLdb.Error, e:
 #       conn.rollback()
        sys.stderr.write("Error %d: %s\n" % (e.args[0],e.args[1]))
        sys.exit(1)

    if options.xml:
        xml_out = open_xml_out(options.xml)
        header="<?xml version="+'"'+"1.0"+'"'+" encoding="+'"'+"iso-8859-1"+'"'+"?>"
        header = header+"<dump recorded=" + '"' + curtime + '"' + "><for>vo:atlas</for>"+"\n"+"<entry-set>"+"\n" 
        xml_out.write(header)   

    if options.json:
        json_out = open_json_out(options.json)
        header = ' { "recorded" : "%s", "for" : "vo:atlas", "entries" : [ \n' % curtime
        json_out.write(header)  

    if options.txt:
        txt_out = open_txt_out(options.txt)

    fileids={}
    first_line = True

    timestamp=0
    if options.date:
        timestamp=int(time.mktime(datetime.datetime.strptime(str(options.date), "%Y%m%d").timetuple()))
        print timestamp
    elif options.age:
        now = int(time.time())
        timestamp=now-86400*options.age

    for row in cursor:
        if options.verbose:
            sys.stderr.write("%s\n" % str(row))

        fileid,parent_fileid,name,filesize,filemode,csumvalue,atime,mtime = row
        if parent_fileid==0:
            fileids[fileid]=''
        else:
            try:
                fileids[fileid]=fileids[parent_fileid]+'/'+name 
            except KeyError:
                sys.stderr.write("The file's parent does not exist. Check your DPM database if there are parents of file with fileid: %s.\n" % str(fileid))
                continue
            except:
                sys.stderr.write("Unkown error occurred for file with fileid: %s.\n" % str(fileid))
                continue

            if int(filemode)>30000:      # To select files
                if mtime>timestamp:
                    continue
                if not fileids[fileid].startswith(options.path):
                    continue
                else:
                    shortened_path = fileids[fileid][len(options.path):]
                if options.txt:
                    content = "%s\n" % (shortened_path)
                    txt_out.write(content)
                if options.xml:
                    content = '''<entry name="%s" size="%s" cs="%s" atime="%s" mtime="%s" />\n''' % (shortened_path, filesize, csumvalue, atime, mtime)
                    xml_out.write(content)
                if options.json:
                    if first_line:
                        first_line = False
                        content = ''
                    else:
                        content = ',\n'
                    content += '{ "name" : "%s", "size" : "%s", "cs" : "%s", "atime" : "%s", "mtime" : "%s" }' % (shortened_path, filesize, csumvalue, atime, mtime)
                    json_out.write(content)

    if options.xml:
        xml_out.write("</entry-set></dump>\n")
        xml_out.close()
    if options.json:
        json_out.write('] }\n')
        json_out.close()
    if options.txt:
        sys.stderr.write("All done!\n")
        txt_out.close()

# Close cursor and connection
    cursor.close()
    conn.close()
        
    

if __name__=="__main__":
    usage = "usage: %prog [options]" 
    description = "Dumps the content of DPM storage element into a file that can be used for consistency check against LFC."
#    epilog = "If OUTPUT_FILE is specified as - (dash) the dump will be printed to standard output."
    parser = OptionParser(usage=usage, description=description)

    guess_nsconfig = guess_config_files()

    parser.add_option("-v", "--verbose", action="store_true", help="Print information messages about what is being done.")
    parser.add_option("-c", "--nsconfig", action="store", help="Path to NSCONFIG. File where sql connection info is stored. Default: %s" % guess_nsconfig, default=guess_nsconfig)
    parser.add_option("-x", "--xml", action="store", help="Create output file in XML format.", metavar="XMLFILE")
    parser.add_option("-j", "--json", action="store", help="Create output file in JSON format.", metavar="JSONFILE")
    parser.add_option("-t", "--txt", action="store", help="Create output file in TXT format.", metavar="TXTFILE")
    parser.add_option("-p", "--path", action="store", help="Dump only files within this DPNS path.", default="/", metavar="PATH")
    parser.add_option("-a", "--age", action="store", help="Dump only files older than AGE days. Default: 30 days", default="30", metavar="AGE")
    parser.add_option("-D", "--date", action="store", help="Dump only files up to the day before date. Format YYYYMMDD", metavar="DATE")

    (options, arguments) = parser.parse_args()
    
    # Convert options to "standard" formats (integers, strings with / at the end ...)
    if not options.path.endswith('/'):
        options.path += '/'
    options.age = int(options.age)

    conn_data = get_conn_data(options.nsconfig, options.verbose)

    dump_data(conn_data, options)


# TODO: guess path based on AGIS


# vi:et:sw=4:ts=4
